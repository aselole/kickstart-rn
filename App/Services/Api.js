import apisauce from 'apisauce'
import { jsonToFormEncodedCaps } from '../Lib/JsonUtils'
import AppConfig from '../Config/AppConfig'

const create = (baseURL = AppConfig.baseServerURL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    timeout: 30000
  })

  api.addRequestTransform(request => {  // rubah format request parameter
    request.data = jsonToFormEncodedCaps(request.data)
  })

  api.addResponseTransform(response => {  // rubah format response parameter
    if(response.data == null) response.ok = false
    else if(response.data.code === 500) response.ok = false
  })

  const signin = (payload) => api.post('UserGobang/login', payload)
  const signup = (payload) => api.post('UserGobang/signup', payload)
  const signupProfil = (payload) => api.post('UserGobang/submitDataProfil', payload)

  // Master data
  const getBangunan = (payload) => api.post('master/getBangunan', payload)
  const getJenisBangunan = (payload) => api.post('master/getJenisBangunan', payload)

  return {
    signin,
    signup,
    signupProfil,
    getBangunan,
    getJenisBangunan
  }
}

export default {
  create
}
