import { call, put } from 'redux-saga/effects'
import UserActions from '../Redux/UserRedux'
import NavActions from '../Redux/NavigationRedux'
import { Strings } from '../Themes/'

export function * signin (api, action) {
  const response = yield call(api.signin, action)

  if (response.ok) {
    if(response.data !== null) yield put(UserActions.signinSuccess(response.data.result))
    else yield put(UserActions.signinFailure())
  } else {
    yield put(UserActions.signinFailure())
  }
}

export function * signup (api, action) {
  const response = yield call(api.signup, action)

  if (response.ok) {
    if(response.data !== null) {
      switch(response.data.code) {
        case 200:
          yield put(UserActions.signupSuccess(response.data.id_detail))
          return;
        case 50:
          yield put(UserActions.signupFailure(Strings.fail_hp_empty))
          return;
        case 100:
          yield put(UserActions.signupFailure(Strings.fail_hp_used))
          return;
        case 150:
          yield put(UserActions.signupFailure(Strings.fail_username))
          return;
        default:
          yield put(UserActions.signupFailure(Strings.fail_signup))
          return;
      }
    }
  }
  yield put(UserActions.signupFailure(Strings.fail_signup))
}

export function * signupProfil (api, action) {
  const response = yield call(api.signupProfil, action)

  if (response.ok) {
    if(response.data !== null) {
      switch(response.data.code) {
        case 200:
          yield put(UserActions.signupProfilSuccess())
          return;
        default:
          yield put(UserActions.signupFailure(Strings.fail_signup))
          return;
      }
    }
  }
  yield put(UserActions.signupFailure(Strings.fail_signup))
}
