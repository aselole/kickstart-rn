import { put, call } from 'redux-saga/effects'
import StartupActions from '../Redux/StartupRedux'

export function * requestMaster (api, action) {
  const { index } = action

  let response = null
  switch(index) {
    case 0: response = yield call(api.getBangunan, action); break;
    case 1: response = yield call(api.getJenisBangunan, action); break;
  }

  if(response.ok) {
    if(response.data !== null) yield put(StartupActions.success(index, response.data.result))
    else yield put(StartupActions.failure(index))
  } else {
    yield put(StartupActions.failure(index))
  }
}
