import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes/'
import stylesTheme, { deviceHeight, deviceWidth, NAV_HEIGHT } from '../../Themes/StyleSub'

export default StyleSheet.create({
  ...stylesTheme,
  toolbar: {
    backgroundColor: Colors.theme2,
    marginTop: NAV_HEIGHT,
    elevation: 10,
    shadowOffset: {
      width: 5,
      height: 5
    },
    shadowRadius: 4,
    shadowOpacity: 0.2,
  }
})
