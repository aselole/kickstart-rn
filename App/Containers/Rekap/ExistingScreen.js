import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Strings, Images } from '../../Themes/'
import NavigationBar from '../../Components/NavigationBar'
import UserActions from '../../Redux/UserRedux'
import BoxSearch from '../../Components/BoxSearch'
import styles from './Styles'

class ExistingScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      term: ''
    }
  }

  _onSearch() {

  }

  _onCancel() {
    this.setState({ term: '' })
  }

  render () {
    return (
      <View style={[styles.normalPage]}>
        <NavigationBar
          navigator={this.props.navigation}
          titleText={Strings.app_title}
          isBack={0}
          rightButtons={
            [
              {
                key: 1,
                buttonIcon: Images.logout,
                buttonAction: (() => this.props.logout()),
                buttonWidth: 22,
                buttonHeight: 22,
              },
            ]
          }
          isElevation={false}
        />
        <View style={styles.toolbar}>
          <BoxSearch
            title={Strings.nama_bangunan}
            onSearch={this._onSearch.bind(this)}
            onCancel={this._onCancel.bind(this)}
            onChangeTerm={(val) => this.setState({term: val})}
            searchTerm={this.state.term} />
        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(UserActions.logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(ExistingScreen)
