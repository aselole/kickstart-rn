import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Strings, Images } from '../../Themes/'
import ReduxNavigation from '../../Navigation/ReduxNavigation'
import UserActions from '../../Redux/UserRedux'
import TextInputIcon from '../../Components/TextInputIcon'
import ButtonRound from '../../Components/ButtonRound'
import TextTouchable from '../../Components/TextTouchable'
import styles from './Styles'

class SignupScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      no_hp: '', username: '',
      password: '', password_repeat: '',
      password_error: false, password_error_msg: '',
      password_repeat_error: false, password_repeat_error_msg: ''
    }
  }

  componentDidMount() {
    this.props.reset()
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.ok != nextProps.ok) {
      if(nextProps.ok === true)
        this.props.navigation.navigate('SignupProfileScreen')
    }
  }

  _signup() {
    if(this.state.password !== this.state.password_repeat) {
      this.setState({ password_repeat_error: true, password_repeat_error_msg: Strings.fail_pass_similar })
      return;
    }
    if(!this.state.password_error && !this.state.password_repeat_error) {
      this.props.signup(
        this.state.username,
        this.state.password,
        this.state.no_hp
      )
    }
  }

  _checkPassword(pass) {
    if(pass.length < 5) {
      this.setState({ password_error: true, password_error_msg: Strings.fail_pass_length })
    } else {
      this.setState({ password_error: false })
    }
  }

  _checkPasswordRepeat(pass) {
    if(pass.length < 5) {
      this.setState({ password_repeat_error: true, password_repeat_error_msg: Strings.fail_pass_length })
    } else {
      this.setState({ password_repeat_error: false })
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <TextInputIcon
            placeholder={Strings.no_hp}
            onChangeText={(no_hp) => this.setState({no_hp})}
            value={this.state.no_hp}
            keyboardType={'phone-pad'}
            image={Images.user} />
        <TextInputIcon
            placeholder={Strings.username}
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
            image={Images.user} />
        <TextInputIcon
            secureTextEntry={true}
            placeholder={Strings.password}
            onChangeText={(password) => {
              this.setState({password})
              this._checkPassword(password)
            }}
            value={this.state.password}
            image={Images.pass}
            error={this.props.password_error}
            errorMsg={this.props.password_error_msg} />
        { this.state.password_error &&
          <Text>{this.state.password_error_msg}</Text>
        }
        <TextInputIcon
            secureTextEntry={true}
            placeholder={Strings.password_rep}
            onChangeText={(password_repeat) => {
              this.setState({password_repeat})
              this._checkPasswordRepeat(password_repeat)
            }}
            value={this.state.password_repeat}
            image={Images.pass}
            error={this.props.password_repeat_error}
            errorMsg={this.props.password_repeat_error_msg} />
        { this.state.password_repeat_error &&
          <Text>{this.state.password_repeat_error_msg}</Text>
        }
        <ButtonRound
          text={Strings.daftar}
          onPress={this._signup.bind(this)}
          isLoading={this.props.fetching} />

        { !this.props.fetching && this.props.error &&
          <Text>{this.props.error_msg}</Text>
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  fetching: state.user.fetching,
  error: state.user.error,
  error_msg: state.user.error_msg,
  id_user: state.user.id_user,
  ok: state.user.ok_signup
})

const mapDispatchToProps = (dispatch) => ({
  reset: () => dispatch(UserActions.stateReset()),
  signup: (username, password, hp) => dispatch(UserActions.signupRequest(username, password, hp))
})

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen)
