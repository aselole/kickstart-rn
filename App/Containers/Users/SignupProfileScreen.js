import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Strings, Images } from '../../Themes/'
import ReduxNavigation from '../../Navigation/ReduxNavigation'
import UserActions from '../../Redux/UserRedux'
import TextInputIcon from '../../Components/TextInputIcon'
import ButtonRound from '../../Components/ButtonRound'
import TextTouchable from '../../Components/TextTouchable'
import styles from './Styles'

class SignupProfileScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      nama: '', kecamatan: '',
      kelurahan: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.ok != nextProps.ok) {
      if(nextProps.ok === true)
        this.props.navigation.navigate('LoginScreen')
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <TextInputIcon
            placeholder={Strings.nama}
            onChangeText={(nama) => this.setState({nama})}
            value={this.state.nama}
            image={Images.user} />
        <TextInputIcon
            placeholder={Strings.kecamatan}
            onChangeText={(kecamatan) => this.setState({kecamatan})}
            value={this.state.kecamatan}
            image={Images.user} />
        <TextInputIcon
            placeholder={Strings.kelurahan}
            onChangeText={(kelurahan) => this.setState({kelurahan})}
            value={this.state.kelurahan}
            image={Images.pass} />
        <ButtonRound
          text={Strings.daftar}
          onPress={() => this.props.signup(this.state.nama, this.state.kecamatan, this.state.kelurahan, this.props.id_user)}
          isLoading={this.props.fetching} />

        { !this.props.fetching && this.props.error &&
          <Text>{this.props.error_msg}</Text>
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  fetching: state.user.fetching,
  error: state.user.error,
  error_msg: state.user.error_msg,
  ok: state.user.ok_signup_profil,
  id_user: state.user.id_user
})

const mapDispatchToProps = (dispatch) => ({
  reset: () => dispatch(UserActions.stateReset()),
  signup: (nama, kecamatan, kelurahan, id_user) => dispatch(UserActions.signupProfilRequest(nama, kecamatan, kelurahan, id_user))
})

export default connect(mapStateToProps, mapDispatchToProps)(SignupProfileScreen)
