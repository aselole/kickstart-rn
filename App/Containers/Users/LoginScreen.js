import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Strings, Images } from '../../Themes/'
import ReduxNavigation from '../../Navigation/ReduxNavigation'
import UserActions from '../../Redux/UserRedux'
import TextInputIcon from '../../Components/TextInputIcon'
import ButtonRound from '../../Components/ButtonRound'
import TextTouchable from '../../Components/TextTouchable'
import styles from './Styles'

class LoginScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '', password: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.ok != nextProps.ok) {
      if(nextProps.ok === true)
        this.props.navigation.navigate('RekapExisting')
    }
  }

  _signup() {
    this.props.navigation.navigate('SignupScreen')
  }

  render () {
    return (
      <View style={styles.container}>
        <TextInputIcon
            placeholder={Strings.username}
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
            image={Images.user} />
        <TextInputIcon
            secureTextEntry={true}
            placeholder={Strings.password}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            image={Images.pass} />
        <ButtonRound
          text={Strings.login}
          onPress={() => this.props.signin(this.state.username, this.state.password)}
          isLoading={this.props.fetching} />

        <Text>{Strings.daftar_belum}</Text>
        <TextTouchable title={Strings.daftar_skr} onPress={this._signup.bind(this)}></TextTouchable>

        { !this.props.fetching && this.props.error &&
          <Text>{Strings.fail_login}</Text>
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  fetching: state.user.fetching,
  error: state.user.error,
  ok: state.user.ok_login
})

const mapDispatchToProps = (dispatch) => ({
  signin: (username, password) => dispatch(UserActions.signinRequest(username, password))
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
