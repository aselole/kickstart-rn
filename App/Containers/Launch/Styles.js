import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'
import stylesTheme from '../../Themes/StyleSub'

export default StyleSheet.create({
  ...stylesTheme,
  mainContainer: {
    flex:1,
    justifyContent:'center'
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  background: {
    flex: 1,
    justifyContent: 'center',
  }
})
