import React, { Component } from 'react'
import { ImageBackground, View, Text } from 'react-native'
import { connect } from 'react-redux'
import { getPercentage } from '../../Lib/NumberUtils'
import { Images, Colors, Strings } from '../../Themes'
import * as Progress from 'react-native-progress'
import { DotIndicator } from 'react-native-indicators'
import ButtonRound from '../../Components/ButtonRound'
import StartupActions from '../../Redux/StartupRedux'
import styles from './Styles'

class LaunchScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      isError: false
    }
  }

  componentDidMount() {
    this._startApp()
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.success !== nextProps.success) {
      if(nextProps.success === false)
        this.setState({ isError: true })
    }
    if(this.props.dataLoaded !== nextProps.dataLoaded) {
      if(this.props.dataCount > 0 && nextProps.dataLoaded === this.props.dataCount)
        this._onFinishSetup()
    }
  }

  _startApp() {
    if(this.props.isSetupFinished) {
      this._onFinishSetup()
    } else {
      for(let i=0; i < this.props.dataCount; i++)
        this.props.masterRequest(i)
    }
  }

  _onFinishSetup() {
    this.props.finishSetup()
    if(this.props.isLoggedin) this.props.navigation.navigate('RekapExisting')
    else this.props.navigation.navigate('LoginScreen')
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground style={styles.background} source={Images.background} resizeMode='stretch'>
          <View style={{ flex:1, flexDirection:'column', alignItems: 'stretch', paddingVertical:40 }}>

            <View style={[ {flex:2, flexDirection:'row', backgroundColor:'transparent', alignItems:'center', justifyContent:'center'}]}>
              <View style={{flex:1, flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                <Text style={[ styles.titleText, styles.semiBold, {fontSize:25, textAlign:'center', backgroundColor:'transparent', color:'white'}]}>
                  {Strings.app_title}
                </Text>
              </View>
            </View>

            <View style={[ {flex:5, flexDirection:'row', alignItems:'center', justifyContent:'center', backgroundColor:'transparent'} ]}>
              { !this.state.isError &&
                <Progress.Circle
                  size={250}
                  thickness={20}
                  showsText={true}
                  textStyle={[ styles.extraBold, {fontWeight:'bold', color:'white'} ]}
                  strokeCap='round'
                  color='white'
                  progress={getPercentage(this.props.dataLoaded, this.props.dataCount)/100} />
              }
              { this.state.isError &&
                <Text adjustsFontSizeToFit={true} style={[ {flex:1, fontSize:17, color:'white', textAlign:'center', backgroundColor:'transparent' } ]}>
                  { Strings.fail_setup_error }
                </Text>
              }
            </View>

            <View style={[ {flex:2, flexDirection:'column', justifyContent:'flex-end', backgroundColor:'transparent'} ]}>

              <View style={{flex:2}}/>
              <View style={[ {flex:1, flexDirection:'row', alignSelf:'flex-end', backgroundColor:'transparent'} ]}>
                <Text adjustsFontSizeToFit={true} style={[ {flex:1, fontSize:17, color:'white', textAlign:'center', backgroundColor:'transparent' } ]}>
                  {Strings.setup_title}
                </Text>
              </View>

              <View style={[ {flex:1, flexDirection: 'row', alignItems:'flex-end', alignSelf:'center', backgroundColor:'transparent'} ]}>
                <Text style={[ styles.regularBold, {fontSize: 14, color:'white', backgroundColor:'transparent', textAlign:'center' } ]}>
                  {this.props.title[this.props.index]}
                </Text>
                <View>
                  <DotIndicator color={Colors.steel} count={4} size={3} style={{position:'absolute', bottom:2, height: null, width:null, backgroundColor:'transparent'}} />
                </View>
              </View>

            </View>

          </View>
        </ImageBackground>
      </View>
    )
  }

}

const mapStateToProps = (state) => ({
  dataLoaded: state.startup.dataLoaded,
  dataCount: state.startup.dataCount,
  index: state.startup.index,
  title: state.startup.title,
  data: state.startup.data,
  fetching: state.startup.fetching,
  loaded: state.startup.loaded,
  success: state.startup.success,
  isSetupFinished: state.startup.ok_setup,
  isLoggedin: state.user.ok_login,
  startup: state.startup.startedup
})

const mapDispatchToProps = (dispatch) => ({
  masterRequest: (index) => dispatch(StartupActions.masterDataRequest(index)),
  finishSetup: () => dispatch(StartupActions.finishSetup())
})

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
