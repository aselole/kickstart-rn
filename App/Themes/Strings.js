const strings = {
  app_title: 'Simbang KUKAR',

  login: 'Login',
  username: 'Username',
  password: 'Password',
  password_rep: 'Ulangi Password',
  no_hp: 'Nomor Handphone',

  fail_login: 'Username/Password tidak cocok, silahkan coba kembali',
  success_login: '',
  daftar_belum: 'Belum Mendaftar?',
  daftar_skr: 'Daftar Sekarang',
  daftar: 'Daftar',

  fail_username: 'Username sudah terdaftar, silahkan ganti username!',
  fail_hp_empty: 'Nomor HP belum didaftarkan, silahkan kontak admin!',
  fail_hp_used: 'Nomor HP sudah didaftarkan untuk user lain, silahkan kontak admin!',
  fail_signup: 'Proses pendaftaran user mengalami masalah',
  fail_pass_similar: 'Pastikan password sama terlebih dahulu',
  fail_pass_length: 'Password harus lebih dari 5 karakter',

  nama_bangunan: 'Nama Bangunan',
  setup_title: 'Sedang menyiapkan aplikasi',
  fail_setup_error: 'setup aplikasi mengalami gangguan'
}

export default strings
