// leave off @2x/@3x
const images = {
  launch: require('../Assets/Images/launch-icon.png'),
  ready: require('../Assets/Images/your-app.png'),
  background: require('../Assets/Images/BG.png'),
  user: require('../Assets/Images/ic_user.png'),
  pass: require('../Assets/Images/ic_password.png'),
  logout: require('../Assets/Images/ic_logout.png'),
  home: require('../Assets/Images/ic_house.png'),
  search: require('../Assets/Images/ic_search.png'),
  cancel: require('../Assets/Images/ic_cancel.png')
}

export default images
