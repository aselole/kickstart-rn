// Simple React Native specific changes

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  baseServerURL: 'http://simbang-kukar.idcosci.com/'
}
