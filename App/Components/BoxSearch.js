import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from '../Themes/'
import Icon from 'react-native-vector-icons/FontAwesome'
import stylesTheme, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
} from '../Themes/StyleSub'

export default class BoxSearch extends Component {

  constructor(props) {
    super(props)
  }

  render () {
    return (
      <View style={[ stylesTheme.itemWhiteBox, styles.mainBox ]}>
        <View style={styles.leftCol}>
          <View style={styles.iconBox}>
            <Image source={this.props.imgLeft} style={{width:30, height:30}} />
          </View>
          <View style={{
            width: 0.7,
            height: 25,
            backgroundColor: 'rgb(229,229,229)'}}
          />
        </View>
        <View style={styles.centerText}>
          <TextInput
            ref='searchText'
            autoFocus
            placeholder={this.props.title}
            underlineColorAndroid='transparent'
            style={styles.searchInput}
            value={this.props.searchTerm}
            onChangeText={this.props.onChangeTerm}
            onSubmitEditing={() => this.props.onSearch(this.props.searchTerm)}
            autoCapitalize='none'
            returnKeyType={'search'}
            autoCorrect={false}
            placeholderTextColor={Colors.themeLight}
            selectionColor={Colors.themeLight}
            textColor={Colors.themeLight}
          />
        </View>
        <TouchableOpacity style={styles.rightCol} onPress={this.props.onCancel}>
          <Image source={this.props.imgRight} style={{width:30, height:30}} />
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: Platform.OS === 'ios' ? 8 : 0,
    paddingBottom: Platform.OS === 'ios' ? 8 : 0,
    marginBottom: 10,
  },
  leftCol: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 45,
  },
  iconBox: {
    width: 37,
  },
  centerText: {
    flex: 1,
  },
  rightCol: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInput: {
    fontSize: 15,
    lineHeight: 12
  }
});

BoxSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onChangeTerm: PropTypes.func.isRequired,
  searchTerm: PropTypes.string,
  title: PropTypes.string,
  imgLeft: PropTypes.number,
  imgRight: PropTypes.number
}

BoxSearch.defaultProps = {
  imgLeft: Images.search,
  imgRight: Images.cancel
}
