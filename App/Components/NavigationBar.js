import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import { Colors, Images } from '../Themes/'
import PropTypes from 'prop-types'

import CommonStyles, {
  deviceWidth,
  deviceHeight,
  themeGradient,
  NAV_HEIGHT,
  shadowOffsetWidth, shadowOffsetHeight, shadowRadius, shadowOpacity
} from '../Themes/StyleSub';

export default class NavigationBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const privateStyles = StyleSheet.create({
      navbar: {
        position: 'absolute',
        top: 0,
        flexDirection: 'row',
        alignItems: 'center',
        width: this.props.navbarStyle.width,
        height: this.props.navbarStyle.height,
        paddingLeft: this.props.navbarStyle.paddingLeft,
        paddingRight: this.props.navbarStyle.paddingRight,
        elevation: this.props.navbarStyle.elevation,
        shadowOffset: {
          width: 5,
          height: 5
        },
        shadowRadius: 4,
        shadowOpacity: 0.2,
      },
      titleText: {
        color: this.props.titleTextStyle.color,
        fontSize: this.props.titleTextStyle.fontSize,
        fontFamily: this.props.titleTextStyle.fontFamily,
      },
    });

    return (
        <View
          style={[ privateStyles.navbar, {
            backgroundColor: this.props.gradientBgStyle.color,
            borderBottomColor: 'rgba(0,0,0,0)',
            borderBottomWidth: 0,
            paddingTop: Platform.OS == 'ios' ? 20 : 0
          } ]}>
          <View style={styles.leftCol}>
            {
              (() => {
                if (this.props.isBack === 1) {
                  return (
                    <View key={0}>
                      <TouchableOpacity
                        onPress={this._onClickMenuButton.bind(this)}>
                        <View style={styles.leftButton}>
                          <Image
                            source={Images.home}
                            style={{width: 23, height: 23}}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  )
                } else if(this.props.isBack === 2) {
                  return (
                    <View key={0}>
                      <TouchableOpacity
                        onPress={this._onClickBackButton.bind(this)}>
                        <View style={styles.leftButton}>
                          <Icon
                            name="md-arrow-back"
                            size={this.props.backIconStyle.size}
                            color={this.props.backIconStyle.color}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  )
                }
              })()
            }
          </View>
          <View style={styles.screenTitleCol}>
            {
              (() => {
                if (this.props.screenTitle !== 'text') {
                  return (
                    <Image
                      source={this.props.titleImg}
                      style={{width: this.props.titleImgStyle.width, height: this.props.titleImgStyle.height}}
                    />
                  )
                } else {
                  return (
                    <Text style={privateStyles.titleText}>
                      {this.props.titleText}
                    </Text>
                  )
                }
              })()
            }
          </View>
          <View style={styles.rightCol}>
            {this.props.rightButtons != null &&
              this.props.rightButtons.map(button => (
                <View key={button.key}>
                  <TouchableOpacity
                    onPress={button.buttonAction}
                  >
                    <View style={{
                      flexDirection: 'row',
                      justifyContent: 'flex-end',
                      alignItems: 'center',
                      height: NAV_HEIGHT,
                      width: button.buttonWidth + 15,
                    }}>
                      <Image
                        source={button.buttonIcon}
                        style={{width: button.buttonWidth, height: button.buttonHeight}}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              ))
            }
          </View>
        </View>
    );
  }

  // Handle click menu button
  _onClickMenuButton() {
    this.props.navigator.navigate('Dashboard')
  }

  // Handle click back button
  _onClickBackButton() {
    if (this.props.isModal == true) {
      this.props.navigator.dismissAllModals({
        animationType: 'slide-down'
      });
    } else {
      this.props.navigator.state.params.refresh();
      this.props.navigator.goBack();
    }
  }
}

const styles = StyleSheet.create({
  leftCol: {
    width: deviceWidth * 1/5 - 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftButton: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 52,
    height: NAV_HEIGHT,
  },
  screenTitleCol: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: deviceWidth * 3/5,
  },
  rightCol: {
    width: deviceWidth * 1/5 - 15,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

NavigationBar.propTypes = {
  isBack: PropTypes.number, // menu icon or back icon in left navbar
  screenTitle: PropTypes.string, // screenTitle to 'text' or 'image'
  navbarStyle: PropTypes.oneOfType([PropTypes.array,PropTypes.object,PropTypes.number]), // styles of navbar
  gradientBgStyle: PropTypes.oneOfType([PropTypes.array,PropTypes.object,PropTypes.number]), // styles of gradient
  titleText: PropTypes.string, // text of titleText
  titleTextStyle: PropTypes.oneOfType([PropTypes.array,PropTypes.object,PropTypes.number]), // styles of titleText
  titleImg: PropTypes.number, // imageUrl of titleImg
  titleImgStyle: PropTypes.oneOfType([PropTypes.array,PropTypes.object,PropTypes.number]), // styles of titleImg
  backIconStyle: PropTypes.oneOfType([PropTypes.array,PropTypes.object,PropTypes.number]), // styles of backIcon
  refreshPrevPage: PropTypes.func,
};

NavigationBar.defaultProps = {
  isBack: 0,
  screenTitle: 'text',
  navbarStyle: {
    width: deviceWidth,
    height: NAV_HEIGHT,
    paddingLeft: 15,
    paddingRight: 15,
    elevation: 10,
  },
  gradientBgStyle: {
    color: themeGradient.colors,
    start: {x: 0.2, y: 0.4},
    end: {x: 1.2, y: 1.0},
  },
  titleTextStyle: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
  },
  titleImgStyle: {},
  backIconStyle: {
    size: 28,
    color: '#FFFFFF',
  },
};
