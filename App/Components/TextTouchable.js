import React, {Component} from "react"
import {Text, View, TouchableHighlight} from "react-native"

import css from "../Themes/Style"

export default class IconInput extends Component {
    render() {
        return (
            <TouchableHighlight {...this.props}>
              <Text style={this.props.textStyle}>{this.props.title}</Text>
            </TouchableHighlight>
        );
    }

}
