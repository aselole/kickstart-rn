import React, {Component} from "react"
import {Text, StyleSheet, Dimensions, TouchableOpacity} from "react-native"
import PropTypes from 'prop-types'
import { Colors, Images, Servers, Strings } from '../Themes/'
import stylesTheme, {shadowOffsetWidth, shadowOffsetHeight, shadowRadius, shadowOpacity} from '../Themes/StyleSub';
const {width} = Dimensions.get("window")

// Loading
import { DotIndicator } from 'react-native-indicators'

export default class ButtonRound extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        style={[styles.buttonRoundBlue, this.props.style, {backgroundColor: this.props.color ? this.props.color : Colors.theme}] }
        disabled={this.props.isLoading}
        onPress={this.props.onPress}>
        { !this.props.isLoading &&
          <Text style={[ stylesTheme.regularBold, styles.buttonRectangularText ]}>{this.props.text && this.props.text.toUpperCase()}</Text>
        }
        { this.props.isLoading &&
          <DotIndicator color={Colors.snow} count={4} size={5} style={{alignSelf: 'center', marginVertical: 12}} />
        }
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  buttonRoundBlue: {
    backgroundColor: Colors.theme,
    paddingTop: 12,
    paddingRight: 12,
    paddingBottom: 12,
    paddingLeft: 12,
    borderColor: "transparent",
    alignSelf: "center",
    borderRadius: 20,
    marginTop: 12,
    marginLeft: 10,
    marginRight: 10,
    width: width - 30,
    elevation: 6,
    shadowOffset: {
      width: 2,
      height: 6
    },
    shadowRadius: 4,
    shadowOpacity: 0.3
  },
  buttonRectangularText: {
    color: "#fff",
    alignSelf: "center",
    fontSize: 16,
  },
})

ButtonRound.propTypes = {
  style: PropTypes.object,
  text: PropTypes.string,
  onPress: PropTypes.func,
  isLoading: PropTypes.bool
};

ButtonRound.defaultProps = {isLoading: false};
