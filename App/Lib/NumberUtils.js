
export function getPercentage(current, total) {
  return Math.ceil( ((current+1)/total) * 100 )
}
