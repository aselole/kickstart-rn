
export function getNormalizeEnum(data) {
  return Object.keys(data).map(value => {
    return {
      value,
      text: data[value]
    };
  });
}

export function getEnumFromArrayObject(data) {
  var output = {};
  for (let i = 0; i < data.length; i++) {
     row = data[i];
     output[ row.ID ] = row.NAMA;
  }
  return output;
}

export function arrayToObject(input) {
  var result = {};
  for (var i=0; i<arr.length; i++) {
    result[arr[i].key] = arr[i].value;
  }
  return result;
}

export function arrayToObjectES6(input) {
  var newObj = Object.assign({}, ...input);
  return newObj;
}

export function filterArrayObject(data, filter) {
  let output = data.filter(function(item) {
     return item.ID_RS_BASE == filter;
  }).map(function(item){
      delete item.ID_RS_BASE;
      return item;
  });
  return output;
}
