
export function updateNestedRedux(nestedObject, index, val1, val2, val3) {
  return nestedObject.updateIn([index], (item) => {
    return item.merge({
        fetching: val1,
        loaded: val2,
        success: val3
    });
  });
}

export function updateNestedDataRedux(nestedObject, index, val1) {
  return nestedObject.updateIn([index], (item) => {
    return item.merge({
        value: val1
    });
  });
}
