import moment from 'moment'

export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_READABLE_FORMAT = 'DD MMM YYYY';
export const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export function stringToDate(date) {
  return moment(date).format(DATE_FORMAT);
}

export function getReadableDate(date) {
  if(date === 'undefined' || date === '' || date === null || date === '0000-00-00 00:00:00')
    return '-';
  else return moment(date).format(DATE_READABLE_FORMAT);
}

export function getReadableWeekdays(date) {
  if(date === 'undefined' || date === '' || date === null || date === '0000-00-00 00:00:00')
    return '';
  else return translateWeekdays(moment(date).isoWeekday());
}

function translateWeekdays(day) {
  switch(day) {
    case 1: return 'Senin';
    case 2: return 'Selasa';
    case 3: return 'Rabu';
    case 4: return 'Kamis';
    case 5: return 'Jumat';
    case 6: return 'Sabtu';
    case 7: return 'Minggu';
  }
}

export function isValidDate(str) {
  if(str === '0000-00-00 00:00:00') return true;
  return moment(str, DATE_TIME_FORMAT, true).isValid();
}
