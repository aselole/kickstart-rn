import { StackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/Launch/LaunchScreen'
import LoginScreen from '../Containers/Users/LoginScreen'
import SignupScreen from '../Containers/Users/SignupScreen'
import SignupProfileScreen from '../Containers/Users/SignupProfileScreen'
import RekapExisting from '../Containers/Rekap/ExistingScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  LaunchScreen: { screen: LaunchScreen },
  LoginScreen: { screen: LoginScreen },
  SignupScreen: { screen: SignupScreen },
  SignupProfileScreen: { screen: SignupProfileScreen },
  RekapExisting: { screen: RekapExisting }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
