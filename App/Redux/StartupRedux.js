import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import update from 'immutability-helper'
import { updateNestedRedux, updateNestedDataRedux } from '../Lib/ImmutableUtils'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  startup: null,
  finishSetup: null,
  masterDataRequest: ['index'],
  success: ['index', 'data'],
  failure: ['index']
})

export const StartupTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  startedup: false,
  ok_setup: false,
  index: 0,
  dataCount: 2,
  dataLoaded: 0,
  fetching: {
    0: null, 1: null
  },
  loaded: {
    0: null, 1: null
  },
  success: {
    0: null, 1: null,
  },
  data: {
    0: null, 1: null
  },
  title: {
    0: 'Data bangunan', 1: 'Jenis bangunan'
  }
})

/* ------------- Reducers ------------- */

export const startup = (state) => state.merge({ startedup: true })

export const finishSetup = (state) => state.merge({ ok_setup: true })

export const request = (state, { index }) =>
  state.merge({
    fetching: state.fetching.set([index], true)
  })

export const success = (state, { index, data }) =>
  state.merge({
    dataLoaded: state.dataLoaded + 1,
    fetching: state.fetching.set([index],  false),
    loaded: state.loaded.set([index], true),
    success: state.success.set([index], true),
    data: state.data.set([index], data)
  })

export const failure = (state, { index }) =>
  state.merge({
    fetching: state.fetching.set([index], false),
    loaded: state.loaded.set([index], true),
    success: state.success.set([index], false)
  })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.STARTUP]: startup,
  [Types.FINISH_SETUP]: finishSetup,
  [Types.MASTER_DATA_REQUEST]: request,
  [Types.SUCCESS]: success,
  [Types.FAILURE]: failure
})
