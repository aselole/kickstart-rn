import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  signinRequest: ['username', 'password'],
  signupRequest: ['username', 'password', 'hp'],
  signupProfilRequest: ['nama', 'kecamatan', 'kelurahan', 'id_user'],
  signupSuccess: ['id_user'],
  signupProfilSuccess: [],
  signupFailure: ['error_msg'],
  signinSuccess: ['data'],
  signinFailure: null,
  stateReset: null,
  logout: null
})

export const UserTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  error: null,
  error_msg: '',
  ok_login: null,
  ok_signup: null,
  ok_signup_profil: null,
  username: null,
  password: null,
  hp: null,
  data: null,
  id_user: null,
  nama: null,
  kecamatan: null,
  kelurahan: null
})

/* ------------- Reducers ------------- */

export const signin = (state, { username, password }) =>
  state.merge({ fetching: true, error: null, username, password })

export const signup = (state, { username, password, hp }) =>
  state.merge({ fetching: true, error: null, username, password, hp })

export const signupProfil = (state, { nama, kecamatan, kelurahan, id_user }) =>
  state.merge({ fetching: true, error: null, nama, kecamatan, kelurahan, id_user })

export const signinSuccess = (state, { data }) =>
  state.merge({ fetching: false, error: false, ok_login: true, data: data, username: null, password: null })

export const signupSuccess = (state, { id_user }) =>
  state.merge({ fetching: false, error: false, ok_signup: true, id_user })

export const signupProfilSuccess = (state) =>
  state.merge({ fetching: false, error: false, ok_signup_profil: true })

export const signinFailure = (state) =>
  state.merge({ fetching: false, error: true, ok_login: false, data: null, username: null, password: null })

export const signupFailure = (state, { error_msg }) =>
  state.merge({ fetching: false, error: true, error_msg })

export const reset = (state) =>
  state.merge({ fetching: null, error: null, username: null, password: null, ok_login: null, ok_signup: null, ok_signup_profil: null })

export const logout = (state) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGNIN_REQUEST]: signin,
  [Types.SIGNUP_REQUEST]: signup,
  [Types.SIGNUP_PROFIL_REQUEST]: signupProfil,
  [Types.SIGNUP_SUCCESS]: signupSuccess,
  [Types.SIGNUP_PROFIL_SUCCESS]: signupProfilSuccess,
  [Types.SIGNUP_FAILURE]: signupFailure,
  [Types.SIGNIN_SUCCESS]: signinSuccess,
  [Types.SIGNIN_FAILURE]: signinFailure,
  [Types.STATE_RESET]: reset,
  [Types.LOGOUT]: logout
})
